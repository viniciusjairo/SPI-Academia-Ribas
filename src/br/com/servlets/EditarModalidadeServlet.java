package br.com.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.javaweb.entity.Modalidade;
import br.com.javaweb.service.ModalidadeService;

@WebServlet("/EditarModalidadeServlet")
public class EditarModalidadeServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7148481498987924741L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub		
		String id = req.getParameter("id");
		Modalidade modalidade = null;
		
		if(id != null){
			ModalidadeService modalidadeservice = new ModalidadeService();
			modalidade = modalidadeservice.buscarId(new Long(id));
			req.setAttribute("modalidade", modalidade);
			req.getRequestDispatcher("editarModalidade.jsp").forward(req, resp);
		}
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		super.doPost(req, resp);
	}

}
