package br.com.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.SystemException;

import br.com.javaweb.entity.Aluno;
import br.com.javaweb.service.AlunoService;

@WebServlet("/RemoverAlunosServlet")
public class RemoverAlunosServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public RemoverAlunosServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");

		if (request.getParameter("id") != null) {
			try {
				String id = request.getParameter("id");
				Aluno aluno = new Aluno(new Long(id));
				AlunoService alunoservice = new AlunoService();
				alunoservice.remover(aluno);

			} catch (SystemException e) {

				e.printStackTrace();

			}


		}
		response.sendRedirect("/webacademia/listarAlunos");

	}

	// protected void doPost(HttpServletRequest request,
	// HttpServletResponse response) throws ServletException, IOException {
	// }

}
