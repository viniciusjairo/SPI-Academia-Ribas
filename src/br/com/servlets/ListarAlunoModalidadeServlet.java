package br.com.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.SystemException;

import br.com.javaweb.entity.AlunoModalidade;
import br.com.javaweb.entity.Modalidade;
import br.com.javaweb.service.AlunoModalidadeService;
import br.com.javaweb.service.ModalidadeService;

@WebServlet("/ListarAlunoModalidadeServlet")
public class ListarAlunoModalidadeServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4461336078777398650L;

	public ListarAlunoModalidadeServlet() {
		super();
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		String id = req.getParameter("id");
		System.out.println("log:" + id);

		Modalidade modalidade = null;

		AlunoModalidade alunomodalidade = null;

		AlunoModalidadeService alunomodalidadeservice = new AlunoModalidadeService();

		List<AlunoModalidade> listaAlunoModalidade = alunomodalidadeservice.listar();
		boolean founded = false;

		if (id != null) {
			ModalidadeService modalidadeservice = new ModalidadeService();
			modalidade = modalidadeservice.buscarId(new Long(id));
			System.out.println("log:" + modalidade.getNome());
			if (!listaAlunoModalidade.isEmpty()) {
				System.out.println("logtrue");
				for (AlunoModalidade alm : listaAlunoModalidade) {
					if (alm.getModalidade().getId().equals(modalidade.getId())) {
						alunomodalidade = alm;
						founded = true;
					}
				}

			}
			if (!founded) {
				alunomodalidade = new AlunoModalidade();
				alunomodalidade.setModalidade(modalidade);
				try {
					alunomodalidadeservice.salvar(alunomodalidade);
				} catch (SystemException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
			System.out.println("log2:" + modalidade.getNome());
			System.out.println("log3" + alunomodalidade.getModalidade().getNome());
			req.setAttribute("alunomodalidades", alunomodalidade);
			req.getRequestDispatcher("listaalunoModalidade.jsp").forward(req, resp);

		}

	}
}
