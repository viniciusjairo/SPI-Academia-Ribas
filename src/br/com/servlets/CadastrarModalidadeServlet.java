package br.com.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.javaweb.entity.Modalidade;
import br.com.javaweb.service.ModalidadeService;

@WebServlet("/CadastrarModalidadeServlet")
public class CadastrarModalidadeServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = -427861748096314008L;

	public CadastrarModalidadeServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Modalidade modalidade = new Modalidade();
		String nome = request.getParameter("nome");
		String id = request.getParameter("id");

		modalidade.setNome(nome);

		ModalidadeService modalidadeservice = new ModalidadeService();
		
		try {
			if (id != null) {
				modalidade.setId(new Long(id));
				modalidadeservice.atualizar(modalidade);
			} else {
				
				modalidadeservice.salvar(modalidade);
			}
		} catch (Exception e) {

			e.printStackTrace();

		}

		response.sendRedirect("/webacademia/listarModalidades");

	}

}
