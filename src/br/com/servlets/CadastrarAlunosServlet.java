package br.com.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.javaweb.entity.Aluno;
import br.com.javaweb.entity.Endereco;
import br.com.javaweb.service.AlunoService;

@WebServlet("/CadastrarAlunosServlet")
public class CadastrarAlunosServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public CadastrarAlunosServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		//SimpleDateFormat formato = new SimpleDateFormat("dd/MM /aaaa");
		request.getRequestDispatcher("cadastrarAluno.jsp").forward(request, response);

	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		Aluno aluno = new Aluno();
		aluno.setEndereco(new Endereco());

		String id = request.getParameter("id");
		String nome = request.getParameter("nome");
		String cpf = request.getParameter("cpf");
		String dataNascimento = request.getParameter("dataNascimento");
		String rua = request.getParameter("rua");
		String numero = request.getParameter("numero");
		String complemento = request.getParameter("complemento");
		String bairro = request.getParameter("bairro");
		String cep = request.getParameter("cep");
		String email = request.getParameter("email");
		String matricula = request.getParameter("matricula");
		String celular = request.getParameter("celular");
		String telFixo = request.getParameter("telfixo");

		aluno.setMatricula(matricula);
		aluno.setNome(nome);
		aluno.setCpf(cpf);
		aluno.setEmail(email);
		aluno.setCelular(celular);
		aluno.setTelFixo(telFixo);

		aluno.getEndereco().setRua(rua);
		aluno.getEndereco().setComplemento(complemento);
		aluno.getEndereco().setCep(cep);
		aluno.getEndereco().setBairro(bairro);
		aluno.getEndereco().setNumero(new Integer(numero));// convertendo String em Integer(new Integer(numero))

		//SimpleDateFormat formato = new SimpleDateFormat("dd/mm/aaaa");

		try {

			aluno.setDataNascimento(dataNascimento);

		} catch (Exception e) {

			e.printStackTrace();

		}

		AlunoService alunoservice = new AlunoService();

		try {

			if (id != null) {

				aluno.setId(new Long(id));
				alunoservice.atualizar(aluno);

			} else {

			}

			alunoservice.salvar(aluno);

		} catch (Exception e) {

			e.printStackTrace();

		}

		response.sendRedirect("/webacademia/listarAlunos");
	}
}
