package br.com.servlets;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.SystemException;

import br.com.javaweb.entity.Aluno;
import br.com.javaweb.entity.AlunoModalidade;
import br.com.javaweb.service.AlunoModalidadeService;
import br.com.javaweb.service.AlunoService;

@WebServlet("/CadastrarAlunoModalidadeServlet")
public class CadastrarAlunoModalidadeServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7239600362321069347L;

	public CadastrarAlunoModalidadeServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String id = request.getParameter("id");
		AlunoService as = new AlunoService();
		List<Aluno> lista = as.listaSemModalidade();
	
		request.setAttribute("id", id);
		request.setAttribute("alunos", lista);
		request.getRequestDispatcher("cadastrarAlunoModalidade.jsp").forward(request, response);

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String id = req.getParameter("id");
		String alunoid = req.getParameter("aluno");
		
		Aluno aluno = null;
		AlunoModalidade am = null;
		AlunoService as = new AlunoService();

		if(alunoid != null){
			aluno = as.buscarId(new Long(alunoid));
			
		}
		
		if(id != null){
			AlunoModalidadeService ams = new AlunoModalidadeService();
			am = ams.buscarId(new Long(id));
			aluno.setAlunomodalidade(am);
		
	
			am.setDataEscricao(new Date());
			List<Aluno> la = am.getAluno();
			la.add(aluno);
			am.setAluno(la);
			
			try {
				ams.atualizar(am);
				as.atualizar(aluno);
				
			} catch (SystemException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		
		req.setAttribute("alunomodalidades",am);
		req.getRequestDispatcher("listaalunoModalidade.jsp").forward(req, resp);

	}

}
