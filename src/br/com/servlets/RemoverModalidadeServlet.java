package br.com.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.SystemException;

import br.com.javaweb.entity.Modalidade;
import br.com.javaweb.service.ModalidadeService;

@WebServlet("/RemoverModalidadeServlet")

public class RemoverModalidadeServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1061526252359558592L;
	
	public RemoverModalidadeServlet(){
		super();
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");

		if (request.getParameter("id") != null) {
			try {
				String id = request.getParameter("id");
				Modalidade modalidade = new Modalidade(new Long(id));
				ModalidadeService modalidadeservice = new ModalidadeService();
				modalidadeservice.remover(modalidade);

			} catch (SystemException e) {

				e.printStackTrace();

			}


		}
		response.sendRedirect("/webacademia/listarModalidades");

	}
	
	
	
	

}
