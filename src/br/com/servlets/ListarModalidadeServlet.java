package br.com.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.javaweb.entity.Modalidade;
import br.com.javaweb.service.ModalidadeService;

@WebServlet("/ListarModalidadeServlet")
public class ListarModalidadeServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4808633362397460616L;
	
	public ListarModalidadeServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ModalidadeService ModalidadeService = new ModalidadeService();
		List<Modalidade> modalidades = ModalidadeService.listar();
		request.setAttribute("modalidades", modalidades);
		request.getRequestDispatcher("listaModalidade.jsp").forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

}

