package br.com.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.javaweb.entity.Aluno;
import br.com.javaweb.service.AlunoService;

@WebServlet("/ListarAlunosServlet")
public class ListarAlunosServlet extends HttpServlet {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 6742714411060068266L;

	public ListarAlunosServlet() {
		super();
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		AlunoService alunoService = new AlunoService();
		List<Aluno> alunos = alunoService.listar();
		request.setAttribute("alunos", alunos);
		request.getRequestDispatcher("listaAluno.jsp").forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

}
