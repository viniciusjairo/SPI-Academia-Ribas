package br.com.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.javaweb.entity.Aluno;
import br.com.javaweb.service.AlunoService;

@WebServlet("/EditarAlunoServlet")
public class EditarAlunoServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3583195946583938000L;

	public EditarAlunoServlet() {
		super();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String id = request.getParameter("id");
		Aluno aluno = null;
		if (id != null) {
			AlunoService alunoservice = new AlunoService();
			aluno = alunoservice.buscarId(new Long(id));
			request.setAttribute("aluno", aluno);
			request.getRequestDispatcher("editarAluno.jsp").forward(request, resp);
		}
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
