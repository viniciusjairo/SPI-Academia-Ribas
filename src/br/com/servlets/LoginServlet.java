package br.com.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.com.javaweb.entity.Usuario;
import br.com.javaweb.service.UsuarioService;
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7085136165699506839L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String login = request.getParameter("login");
		String senha = request.getParameter("senha");
		
		if(login.equals("") || senha.equals("")){
			request.setAttribute("msg", "preencha os campos");
			request.getRequestDispatcher("index.jsp").forward(request, response);
			return;
		}
		
		UsuarioService usuarioService = new UsuarioService();
		
		Usuario usuario = usuarioService.efetuarLogin(login, senha);
		
		if(usuario == null){
			request.setAttribute("msg", "login ou senha inválidos");
			request.getRequestDispatcher("index.jsp").forward(request, response);
			return;
		}
		
		HttpSession session = request.getSession();
		session.setAttribute("usuario", usuario);
		
		
		
		response.sendRedirect("/webacademia/listarAlunos");
		
		
		
		
			
		
		
	}
}
