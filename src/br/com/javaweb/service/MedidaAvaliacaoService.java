package br.com.javaweb.service;

import javax.persistence.EntityManager;

import br.com.javaweb.DAO.MedidaAvaliacaoDAO;
import br.com.javaweb.entity.MedidaAvaliacao;

public class MedidaAvaliacaoService extends	GenericoService<MedidaAvaliacao, MedidaAvaliacaoDAO> {

	@Override
	protected MedidaAvaliacaoDAO newDAO(EntityManager manag) {
		return new MedidaAvaliacaoDAO(manag);
	}

}
