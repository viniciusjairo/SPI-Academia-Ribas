package br.com.javaweb.service;

import javax.persistence.EntityManager;
import javax.transaction.SystemException;

import br.com.javaweb.DAO.FichaDAO;
import br.com.javaweb.entity.Ficha;

public class FichaService extends GenericoService<Ficha, FichaDAO> {

	@Override
	protected FichaDAO newDAO(EntityManager manag) {
		return new FichaDAO(manag);
	}
	
	public void salva(Ficha ficha) throws SystemException{
		
		if(ficha.getInstrutor() == null){
			
			throw new SystemException("Ficha precisa de instrutor");
		}
		
		if(ficha.getFichaExercicio() == null || ficha.getFichaExercicio().isEmpty()){
			
			throw new SystemException("Ficha precisa de pelo menos de um exercicio");
		}
		
		super.salvar(ficha);
	}

}
