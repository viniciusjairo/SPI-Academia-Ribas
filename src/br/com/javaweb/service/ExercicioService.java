package br.com.javaweb.service;

import javax.persistence.EntityManager;

import br.com.javaweb.DAO.ExercicioDAO;
import br.com.javaweb.entity.Exercicio;

public class ExercicioService extends GenericoService<Exercicio, ExercicioDAO> {

	@Override
	protected ExercicioDAO newDAO(EntityManager manag) {
		return new ExercicioDAO(manag);
	}

}
