package br.com.javaweb.service;

import javax.persistence.EntityManager;

import br.com.javaweb.DAO.PessoaDAO;
import br.com.javaweb.entity.Pessoa;

public class PessoaService extends GenericoService<Pessoa, PessoaDAO> {

	@Override
	protected PessoaDAO newDAO(EntityManager manag) {
		return new PessoaDAO(manag);
	}

}
