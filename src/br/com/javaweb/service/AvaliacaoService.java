package br.com.javaweb.service;

import javax.persistence.EntityManager;

import br.com.javaweb.DAO.AvaliacaoDAO;
import br.com.javaweb.entity.Avaliacao;

public class AvaliacaoService extends GenericoService<Avaliacao, AvaliacaoDAO> {

	@Override
	protected AvaliacaoDAO newDAO(EntityManager manag) {

		return new AvaliacaoDAO(manag);
	}

}
