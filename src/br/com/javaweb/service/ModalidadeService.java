package br.com.javaweb.service;

import javax.persistence.EntityManager;

import br.com.javaweb.DAO.ModalidadeDAO;
import br.com.javaweb.entity.Modalidade;

public class ModalidadeService extends GenericoService<Modalidade, ModalidadeDAO> {

	@Override
	protected ModalidadeDAO newDAO(EntityManager manag) {
		return new ModalidadeDAO(manag);
	}

}
