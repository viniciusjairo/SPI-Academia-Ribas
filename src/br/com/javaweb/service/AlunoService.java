package br.com.javaweb.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import br.com.javaweb.DAO.AlunoDAO;
import br.com.javaweb.entity.Aluno;

public class AlunoService extends GenericoService<Aluno, AlunoDAO> {

	@Override
	protected AlunoDAO newDAO(EntityManager manag) {
		return new AlunoDAO(manag);
	}
	
	public List<Aluno> listaSemModalidade(){
		AlunoService as = new AlunoService();
		List<Aluno> lista = as.listar();
		List<Aluno> listafinal = new ArrayList<Aluno>();

		for(Aluno aluno: lista){
			if(aluno.getAlunomodalidade() == null){
				
				listafinal.add(aluno);

			}
				
		}
			
		return listafinal;
	}

}
