package br.com.javaweb.service;

import javax.persistence.EntityManager;

import br.com.javaweb.DAO.InstrutorDAO;
import br.com.javaweb.entity.Instrutor;

public class InstrutorService extends GenericoService<Instrutor, InstrutorDAO> {

	@Override
	protected InstrutorDAO newDAO(EntityManager manag) {
		return new InstrutorDAO(manag);
	}

}
