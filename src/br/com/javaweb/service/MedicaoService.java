package br.com.javaweb.service;

import javax.persistence.EntityManager;

import br.com.javaweb.DAO.MedicaoDAO;
import br.com.javaweb.entity.Medicao;

public class MedicaoService extends GenericoService<Medicao, MedicaoDAO> {

	@Override
	protected MedicaoDAO newDAO(EntityManager manag) {
		return new MedicaoDAO(manag);
	}

}