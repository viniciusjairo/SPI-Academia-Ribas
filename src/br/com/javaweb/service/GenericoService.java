package br.com.javaweb.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.transaction.SystemException;
import javax.xml.bind.ValidationException;

import br.com.javaweb.DAO.GenericoDAO;
import br.com.javaweb.entity.GenericoEntity;

public abstract class GenericoService<T extends GenericoEntity, D extends GenericoDAO<T>> {

	@SuppressWarnings("static-access")
	static EntityManagerFactory fac = new Persistence().createEntityManagerFactory("academiamysql");

	protected EntityManager manag;

	protected abstract D newDAO(EntityManager manag);

	public void salvar(T entity) throws SystemException {

		manag = fac.createEntityManager();

		try {

			manag.getTransaction().begin();
			D dao = newDAO(manag);
			dao.salvar(entity);
			manag.getTransaction().commit();
		} catch (Exception e) {

			manag.getTransaction().rollback();
			throw new SystemException("Erro ao salvar");
		}

		finally {
			manag.close();
		}
	}

	public void remover(T entity) throws SystemException {

		manag = fac.createEntityManager();

		try {

			manag.getTransaction().begin();
			if (entity.getId() == null || entity.getId() == 0) {

				throw new ValidationException("Id � obrigat�tio");
			}

			D dao = newDAO(manag);
			entity = dao.buscarId(entity.getId());
			dao.remolver(entity);
			manag.getTransaction().commit();

		} catch (Exception e) {

			manag.getTransaction().rollback();
			throw new SystemException("erro ao remover");
		} finally {

			manag.close();
		}

	}

	public void atualizar(T entity) throws SystemException {

		manag = fac.createEntityManager();
		try {

			manag.getTransaction().begin();
			D dao = newDAO(manag);
			dao.atualizar(entity);
			manag.getTransaction().commit();
		} catch (Exception e) {

			manag.getTransaction().rollback();
			throw new SystemException("Erro ao atualizar");
		}

		finally {
			manag.close();
		}
	}

	public T buscarId(Long id) {

		manag = fac.createEntityManager();
		return newDAO(manag).buscarId(id);

	}

	public List<T> listar() {

		manag = fac.createEntityManager();
		return newDAO(manag).listar();
	}

}
