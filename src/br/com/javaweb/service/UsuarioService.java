package br.com.javaweb.service;

import javax.persistence.EntityManager;

import br.com.javaweb.DAO.UsuarioDAO;
import br.com.javaweb.entity.Usuario;

public class UsuarioService extends GenericoService<Usuario, UsuarioDAO> {

	@Override
	protected UsuarioDAO newDAO(EntityManager manag) {
		return null;
	}
	
public Usuario efetuarLogin(String login, String senha){
		
		manag = fac.createEntityManager();
		UsuarioDAO usuarioDAO = new UsuarioDAO(manag);
		return usuarioDAO.efetuarLogin(login, senha);
		
	}

}
