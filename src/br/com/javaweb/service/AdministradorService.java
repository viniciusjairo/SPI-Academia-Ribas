package br.com.javaweb.service;

import javax.persistence.EntityManager;

import br.com.javaweb.DAO.AdministradorDAO;
import br.com.javaweb.entity.Administrador;

public class AdministradorService extends GenericoService<Administrador, AdministradorDAO> {

	@Override
	protected AdministradorDAO newDAO(EntityManager manag) {
		return new AdministradorDAO(manag);
	}

}
