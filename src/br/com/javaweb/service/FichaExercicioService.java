package br.com.javaweb.service;

import javax.persistence.EntityManager;

import br.com.javaweb.DAO.FichaExercicioDAO;
import br.com.javaweb.entity.FichaExercicio;

public class FichaExercicioService extends GenericoService<FichaExercicio, FichaExercicioDAO> {

	@Override
	protected FichaExercicioDAO newDAO(EntityManager manag) {
		return new FichaExercicioDAO(manag);
	}

}
