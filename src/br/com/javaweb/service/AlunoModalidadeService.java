package br.com.javaweb.service;

import javax.persistence.EntityManager;

import br.com.javaweb.DAO.AlunoModalidadeDAO;
import br.com.javaweb.entity.AlunoModalidade;

public class AlunoModalidadeService extends
		GenericoService<AlunoModalidade, AlunoModalidadeDAO> {

	@Override
	protected AlunoModalidadeDAO newDAO(EntityManager manag) {
		return new AlunoModalidadeDAO(manag);
	}
	

}
