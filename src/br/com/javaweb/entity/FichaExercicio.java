package br.com.javaweb.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity

public class FichaExercicio extends GenericoEntity {

	@Id
	@GeneratedValue
	private Long id;

	private Integer quantRepeticoes;
	private Integer quantSerie;

	@ManyToOne
	private Exercicio exercicio;

	@ManyToOne
	private Ficha ficha;

	public Ficha getFicha() {
		return ficha;
	}

	public void setFicha(Ficha ficha) {
		this.ficha = ficha;
	}

	public Exercicio getExercicio() {
		return exercicio;
	}

	public void setExercicio(Exercicio exercicio) {
		this.exercicio = exercicio;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getQuantRepeticoes() {
		return quantRepeticoes;
	}

	public void setQuantRepeticoes(Integer quantRepeticoes) {
		this.quantRepeticoes = quantRepeticoes;
	}

	public Integer getQuantSerie() {
		return quantSerie;
	}

	public void setQuantSerie(Integer quantSerie) {
		this.quantSerie = quantSerie;
	}


}
