package br.com.javaweb.entity;

import javax.persistence.Entity;

@Entity
public class Administrador extends Pessoa {

	private double salario;

	public double getSalario() {
		return salario;
	}

	public void setSalario(double salario) {
		this.salario = salario;
	}

}
