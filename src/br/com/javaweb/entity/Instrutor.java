package br.com.javaweb.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;

@Entity
public class Instrutor extends Pessoa {

	private String horario;
	private double salario;
	
	@OneToMany(mappedBy = "instrutor")
	private List<MedidaAvaliacao> medidasAvaliacao;

	public List<MedidaAvaliacao> getMedidasAvaliacao() {
		return medidasAvaliacao;
	}

	public void setMedidasAvaliacao(List<MedidaAvaliacao> medidasAvaliacao) {
		this.medidasAvaliacao = medidasAvaliacao;
	}

	public double getSalario() {
		return salario;
	}

	public void setSalario(double salario) {
		this.salario = salario;
	}

	public String getHorario() {
		return horario;
	}

	public void setHorario(String horario) {
		this.horario = horario;
	}

}
