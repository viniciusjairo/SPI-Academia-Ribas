package br.com.javaweb.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity

public class Medicao extends GenericoEntity{
	
	@Id
	@GeneratedValue
	private Long id;
	
	private String nomeMedicao;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNomeMedicao() {
		return nomeMedicao;
	}

	public void setNomeMedicao(String nomeMedicao) {
		this.nomeMedicao = nomeMedicao;
	}

	
	

}
