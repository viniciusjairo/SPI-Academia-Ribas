package br.com.javaweb.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity

public class Ficha extends GenericoEntity{

	@Id
	@GeneratedValue
	private Long id;

	@Temporal(TemporalType.DATE)
	private Date data;

	private String periodo;

	@OneToMany(mappedBy = "ficha")
	private List<FichaExercicio> fichaExercicio;

	@ManyToOne
	private Instrutor instrutor;

	@ManyToOne
	private AlunoModalidade alunomodalidade;

	public Instrutor getInstrutor() {
		return instrutor;
	}

	public void setInstrutor(Instrutor instrutor) {
		this.instrutor = instrutor;
	}

	public List<FichaExercicio> getFichaExercicio() {
		return fichaExercicio;
	}

	public void setFichaExercicio(List<FichaExercicio> fichaExercicio) {
		this.fichaExercicio = fichaExercicio;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public String getPeriodo() {
		return periodo;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

}
