package br.com.javaweb.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Modalidade extends GenericoEntity {
	
	@Id
	@GeneratedValue
	private Long id;
	private String nome;
	
	public Modalidade(){
		
	}
	public Modalidade(long id){
		this.id = id;
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}
