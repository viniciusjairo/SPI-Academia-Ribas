package br.com.javaweb.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class AlunoModalidade extends GenericoEntity {

	@Id
	@GeneratedValue
	private Long id;

	@Temporal(TemporalType.DATE)
	private Date dataEscricao;

	@OneToMany(mappedBy = "alunomodalidade")
	private List<Ficha> ficha;

	@OneToMany(cascade=CascadeType.ALL)
	private List<Aluno> aluno;

	@ManyToOne(cascade=CascadeType.REMOVE)
	private Modalidade modalidade;
	
	
	
	

	public Modalidade getModalidade() {
		return modalidade;
	}

	public void setModalidade(Modalidade modalidade) {
		this.modalidade = modalidade;
	}

	public List<Ficha> getFicha() {
		return ficha;
	}

	public void setFicha(List<Ficha> ficha) {
		this.ficha = ficha;
	}

	public List<Aluno> getAluno() {
		return aluno;
	}

	public void setAluno(List<Aluno> aluno) {
		this.aluno = aluno;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDataEscricao() {
		return dataEscricao;
	}

	public void setDataEscricao(Date dataEscricao) {
		this.dataEscricao = dataEscricao;
	}

}
