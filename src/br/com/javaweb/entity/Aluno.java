package br.com.javaweb.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Aluno extends Pessoa {

	private String matricula;

	public Aluno() {

	}

	public Aluno(Long id) {

		this.id = id;

	}

	public AlunoModalidade getAlunomodalidade() {
		return alunomodalidade;
	}

	public void setAlunomodalidade(AlunoModalidade alunomodalidade) {
		this.alunomodalidade = alunomodalidade;
	}

	@OneToMany(mappedBy = "aluno")
	private List<Avaliacao> avaliacoes;

	@ManyToOne(cascade=CascadeType.ALL)
	private AlunoModalidade alunomodalidade;

	public List<Avaliacao> getAvaliacoes() {
		return avaliacoes;
	}

	public void setAvaliacoes(List<Avaliacao> avaliacoes) {
		this.avaliacoes = avaliacoes;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

}
