
package br.com.javaweb.entity;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToOne;

@Entity

@Inheritance(strategy = InheritanceType.JOINED)
public class Pessoa extends GenericoEntity{

	@Id
	@GeneratedValue
	protected Long id;

	private String nome;
	private String email;
	private String cpf;
	private String telFixo;
	private String celular;

	
	private String dataNascimento;

	@OneToOne
	private Usuario usuario;

	@Embedded
	private Endereco endereco;

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Pessoa() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getTelFixo() {
		return telFixo;
	}

	public void setTelFixo(String telFixo) {
		this.telFixo = telFixo;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;

	}

	// @Override
	// public String toString() {
	// return "Pessoa [id=" + id + ", nome=" + nome + ", email=" + email + ",
	// cpf=" + cpf + ", telFixo=" + telFixo
	// + ", celular=" + celular + ", dataNascimento=" + dataNascimento + "]";
	// }

}
