package br.com.javaweb.DAO;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.javaweb.entity.GenericoEntity;

public abstract class GenericoDAO<T extends GenericoEntity> {

	protected EntityManager manag;

	protected abstract Class<T> entityClass();

	public GenericoDAO(EntityManager manag) {
		this.manag = manag;
	}

	public void salvar(T entity) {
		manag.persist(entity);

	}

	public void remolver(T entity) {
		entity = manag.find(entityClass(), entity.getId());
		manag.remove(entity);

	}

	public void atualizar(T entity) {
		manag.merge(entity);
	}

	@SuppressWarnings("unchecked")
	public List<T> listar() {
		Query query = manag.createQuery("select a from " + entityClass().getSimpleName() + " a ");
		return query.getResultList();
	}

	public T buscarId(Long id) {
		return manag.find(entityClass(), id);
	}

}
