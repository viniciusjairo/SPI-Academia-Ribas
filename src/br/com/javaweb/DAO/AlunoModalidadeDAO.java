package br.com.javaweb.DAO;

import javax.persistence.EntityManager;

import br.com.javaweb.entity.AlunoModalidade;

public class AlunoModalidadeDAO extends GenericoDAO<AlunoModalidade>{

	public AlunoModalidadeDAO(EntityManager manag) {
		super(manag);
	}

	@Override
	protected Class<AlunoModalidade> entityClass() {
		return AlunoModalidade.class;
	}

}
