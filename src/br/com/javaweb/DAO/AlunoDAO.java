package br.com.javaweb.DAO;

import javax.persistence.EntityManager;

import br.com.javaweb.entity.Aluno;

public class AlunoDAO extends GenericoDAO<Aluno> {

	public AlunoDAO(EntityManager manag) {
		super(manag);
	}

	@Override
	protected Class<Aluno> entityClass() {
		return Aluno.class;
	}

}
