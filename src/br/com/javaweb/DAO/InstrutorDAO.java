package br.com.javaweb.DAO;

import javax.persistence.EntityManager;

import br.com.javaweb.entity.Instrutor;

public class InstrutorDAO extends GenericoDAO<Instrutor> {

	public InstrutorDAO(EntityManager manag) {
		super(manag);
	}

	@Override
	protected Class<Instrutor> entityClass() {
		return null;
	}

}
