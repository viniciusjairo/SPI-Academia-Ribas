package br.com.javaweb.DAO;

import javax.persistence.EntityManager;

import br.com.javaweb.entity.Pessoa;

public class PessoaDAO extends GenericoDAO<Pessoa> {

	public PessoaDAO(EntityManager manag) {
		super(manag);
	}

	@Override
	protected Class<Pessoa> entityClass() {
		return Pessoa.class;
	}

}
