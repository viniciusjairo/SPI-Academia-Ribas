package br.com.javaweb.DAO;

import javax.persistence.EntityManager;

import br.com.javaweb.entity.FichaExercicio;

public class FichaExercicioDAO extends GenericoDAO<FichaExercicio> {

	public FichaExercicioDAO(EntityManager manag) {
		super(manag);
	}

	@Override
	protected Class<FichaExercicio> entityClass() {
		return FichaExercicio.class;
	}

}
