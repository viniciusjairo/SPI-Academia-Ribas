package br.com.javaweb.DAO;

import javax.persistence.EntityManager;

import br.com.javaweb.entity.Ficha;

public class FichaDAO extends GenericoDAO<Ficha> {

	public FichaDAO(EntityManager manag) {
		super(manag);
	}

	@Override
	protected Class<Ficha> entityClass() {
		return null;
	}

}
