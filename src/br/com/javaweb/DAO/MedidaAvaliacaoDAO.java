package br.com.javaweb.DAO;

import javax.persistence.EntityManager;

import br.com.javaweb.entity.MedidaAvaliacao;

public class MedidaAvaliacaoDAO extends GenericoDAO<MedidaAvaliacao> {

	public MedidaAvaliacaoDAO(EntityManager manag) {
		super(manag);
	}

	@Override
	protected Class<MedidaAvaliacao> entityClass() {
		return MedidaAvaliacao.class;
	}

}
