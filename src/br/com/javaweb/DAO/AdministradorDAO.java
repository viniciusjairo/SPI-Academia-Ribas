package br.com.javaweb.DAO;

import javax.persistence.EntityManager;

import br.com.javaweb.entity.Administrador;

public class AdministradorDAO extends GenericoDAO<Administrador> {

	public AdministradorDAO(EntityManager manag) {
		super(manag);
	}

	@Override
	protected Class<Administrador> entityClass() {
		return null;
	}

}
