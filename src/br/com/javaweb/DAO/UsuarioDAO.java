package br.com.javaweb.DAO;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.javaweb.entity.Usuario;

public class UsuarioDAO extends GenericoDAO<Usuario> {

	public UsuarioDAO(EntityManager manag) {
		super(manag);
	}

	@Override
	protected Class<Usuario> entityClass() {
		return Usuario.class;
	}
	
	public Usuario efetuarLogin(String login, String senha){
		Query  query = manag.createQuery("select u from Usuario u where u.login = :login and u.senha = :senha");
		query.setParameter("login", login);
		query.setParameter("senha", senha);
		@SuppressWarnings("unchecked")
		List<Usuario> usuarios = query.getResultList();
		
		if(usuarios == null || usuarios.isEmpty()){
			return null;
		}
		return usuarios.get(0);
	}

}
