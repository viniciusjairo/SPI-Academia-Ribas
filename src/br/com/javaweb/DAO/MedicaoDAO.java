package br.com.javaweb.DAO;

import javax.persistence.EntityManager;

import br.com.javaweb.entity.Medicao;

public class MedicaoDAO extends GenericoDAO<Medicao> {

	public MedicaoDAO(EntityManager manag) {
		super(manag);
	}

	@Override
	protected Class<Medicao> entityClass() {
		return Medicao.class;
	}

}
