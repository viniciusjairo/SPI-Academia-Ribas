package br.com.javaweb.DAO;

import javax.persistence.EntityManager;

import br.com.javaweb.entity.Modalidade;

public class ModalidadeDAO extends GenericoDAO<Modalidade> {

	public ModalidadeDAO(EntityManager manag) {
		super(manag);
	}

	@Override
	protected Class<Modalidade> entityClass() {
		return Modalidade.class;
	}

}
