package br.com.javaweb.DAO;

import javax.persistence.EntityManager;

import br.com.javaweb.entity.Exercicio;

public class ExercicioDAO extends GenericoDAO<Exercicio>{

	public ExercicioDAO(EntityManager manag) {
		super(manag);
	}

	@Override
	protected Class<Exercicio> entityClass() {
		return Exercicio.class;
	}

}
