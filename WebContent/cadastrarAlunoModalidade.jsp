<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@page import="br.com.javaweb.entity.Aluno,java.util.List"%>    
	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

	<div>
		<h1>Nova Mátricula</h1>
	</div>

	<div>
	<%List<Aluno> alunos = (List<Aluno>) request.getAttribute("alunos");  %>
	<%String id = (String) request.getAttribute("id"); %>
		<h2>Modalidade</h2>
		<form method="post" action="/webacademia/cadastrarAlunoModalidade" >
			<input type="hidden" name="id" value=<%=id%>> 
			Aluno:<select name="aluno">
				
				<%for(Aluno aluno : alunos){ %>
				<option value=<%=aluno.getId()%>><%=aluno.getNome()%></option>
				<%}%>
			</select> 
			
			<input type="submit" value="Matricular">
			
		</form>
		>
	</div>
</body>
</html>