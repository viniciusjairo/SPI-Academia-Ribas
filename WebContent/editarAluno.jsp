<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="br.com.javaweb.entity.Aluno"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Editar Aluno</title>
</head>
<body>
	<form method='post' action='/webacademia/cadastrarAlunos'>
		<%
			Aluno aluno = (Aluno) request.getAttribute("aluno");
		%>
		<%
			if (aluno != null) {
				
		%>
		<input type="hidden" name="id" value=<%=aluno.getId()%>>

		<table>
			<tr>
				<td>Nome:</td>
				<td><input type='text' name='nome' value='<%=aluno.getNome()%>' /></td>
			</tr>
			<tr>
				<td>CPF:</td>
				<td><input type='text' name='cpf' value=<%=aluno.getCpf()%>></td>
			</tr>
			<tr>
				<td>dataNascimento:</td>
				<td><input type='text' name='dataNascimento'
					value='<%=aluno.getDataNascimento()%>'></td>
			</tr>
			<tr>
				<td>Rua:</td>
				<td><input type='text' name='rua'
					value='<%=aluno.getEndereco().getRua()%>'></td>
			</tr>

			<tr>
				<td>Numero:</td>
				<td><input type='text' name='numero'
					value='<%=aluno.getEndereco().getNumero()%>'></td>
			</tr>

			<tr>
				<td>Complemento:</td>
				<td><input type='text' name='complemento'
					value='<%=aluno.getEndereco().getComplemento()%>'></td>
			</tr>

			<tr>
				<td>Bairro:</td>
				<td><input type='text' name='bairro'
					value='<%=aluno.getEndereco().getBairro()%>'></td>
			</tr>

			<tr>
				<td>Cep:</td>
				<td><input type='text' name='cep'
					value='<%=aluno.getEndereco().getCep()%>'></td>
			</tr>

			<tr>
				<td>Email:</td>
				<td><input type='text' name='email'
					value='<%=aluno.getEmail()%>'></td>
			</tr>

			<tr>
				<td>Matricula:</td>
				<td><input type='text' name='matricula'
					value='<%=aluno.getMatricula()%>'></td>
			</tr>

			<tr>
				<td>Celular:</td>
				<td><input type='text' name='celular'
					value='<%=aluno.getCelular()%>'></td>
			</tr>

			<tr>
				<td>TelFixo:</td>
				<td><input type='text' name='telfixo'
					value='<%=aluno.getTelFixo()%>'></td>
			</tr>

			<tr>
				<td></td>
				<td><input type='submit' value='Salvar'></td>
			</tr>

		</table>

		<%
			}
		%>
	</form>

</body>
</html>