<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="br.com.javaweb.entity.Aluno"%>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Cadastrar Aluno</title>
</head>
<body>
	<form method='post' action='/webacademia/cadastrarAlunos'>

		<table>
			<tr>
				<td>Nome:</td>
				<td><input type='text' name='nome'/></td>
			</tr>
			<tr>
				<td>CPF:</td>
				<td><input type='text' name='cpf'></td>
			</tr>
			<tr>
				<td>dataNascimento:</td>
				<td><input type='text' name='dataNascimento'></td>
			</tr>
			<tr>
				<td>Rua:</td>
				<td><input type='text' name='rua'></td>
			</tr>

			<tr>
				<td>Numero:</td>
				<td><input type='text' name='numero'></td>
			</tr>

			<tr>
				<td>Complemento:</td>
				<td><input type='text' name='complemento'></td>
			</tr>

			<tr>
				<td>Bairro:</td>
				<td><input type='text' name='bairro'></td>
			</tr>

			<tr>
				<td>Cep:</td>
				<td><input type='text' name='cep'></td>
			</tr>

			<tr>
				<td>Email:</td>
				<td><input type='text' name='email'></td>
			</tr>

			<tr>
				<td>Matricula:</td>
				<td><input type='text' name='matricula'></td>
			</tr>

			<tr>
				<td>Celular:</td>
				<td><input type='text' name='celular'></td>
			</tr>

			<tr>
				<td>TelFixo:</td>
				<td><input type='text' name='telfixo'></td>
			</tr>

			<tr>
				<td></td>
				<td><input type='submit' value='Salvar'></td>
			</tr>

		</table>

	</form>
</body>
</html>