<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="br.com.javaweb.entity.AlunoModalidade,br.com.javaweb.entity.Aluno,java.util.List"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Alunos Matriculados</title>
</head>
<body>
	<div>
		<ul>
			<li><a href="/webacademia/listarAlunos">Alunos</a></li>
			<li><a href="/webacademia/listarModalidade">Modalidades</a></li>
			<li><a href="#">Exercicios</a></li>
			<li><a href="#">Medidas</a></li>
		</ul>
	</div>
	<%
		AlunoModalidade alunomodalidade = (AlunoModalidade) request.getAttribute("alunomodalidades");
		
		List<Aluno> ListaAlunos = alunomodalidade.getAluno();
		
	%>
	<div>
		<h1>
			Alunos Matriculados em:
			<%=alunomodalidade.getModalidade().getNome()%>
		</h1>
	</div>
	<div>

		Filtrar por: Nome:<input type="text" name="nome"> CPF: <input
			type="text" name="cpf">
	</div>

	<div>
		<a href='/webacademia/cadastrarAlunoModalidade?id=<%=alunomodalidade.getId()%>'>Nova Mátricula</a>
	</div>

	<table>
		<tr>
			<td>Nome</td>
			<td>Email</td>
			<td>cpf</td>
			<td>Data nascimento</td>
			<td>Ações</td>
		</tr>
		<%if(ListaAlunos != null){ %>
		<% for (Aluno aluno : ListaAlunos){ %>
		<tr>
			<td><%=aluno.getNome()%></td>
			<td><%=aluno.getCpf()%></td>
			<td><%=aluno.getEmail()%></td>
			<td><%=aluno.getDataNascimento()%></td>

		</tr>
	<%}} %>
	</table>

</body>
</html>